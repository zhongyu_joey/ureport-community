package com.bstek.ureport.chart.dataset.impl;

public class AreaDataSet extends BarDataSet{

	@Override
	public String getType() {
		return "area";
	}
}
