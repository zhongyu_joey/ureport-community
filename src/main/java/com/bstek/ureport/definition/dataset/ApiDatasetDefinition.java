package com.bstek.ureport.definition.dataset;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.bstek.ureport.build.Dataset;

public class ApiDatasetDefinition implements DatasetDefinition {

	private static final long serialVersionUID = -7238059187866936269L;

	private String name;
	
	private List<Field> fields;
	
	private RestTemplate restTemplate = new RestTemplate();
	
	public Dataset buildDataset(String dataSourceName, String url, HttpHeaders headers,Map<String, Object> parameters) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dataSourceName", dataSourceName);
		params.put("dataSetName", name);
		params.put("parameters", parameters);
		HttpEntity<Object> entity = new HttpEntity<>(params, headers);
		ResponseEntity<ApiResult> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, ApiResult.class);
		ApiResult result = responseEntity.getBody();
		return new Dataset(name, result.getData());
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<Field> getFields() {
		return fields;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}
}
