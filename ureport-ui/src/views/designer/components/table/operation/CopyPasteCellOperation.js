import Handsontable from 'handsontable'
import { Message } from 'element-ui'
import { undoManager } from '../../Utils.js'

let Interval = null;
let copyData = null;
export function copyCell(cut) {
  const selected = this.getSelected()
  var rowIndex = selected[0]
  var colIndex = selected[1]
  var row2Index = selected[2]
  var col2Index = selected[3]
  const mergeCells = this.getSettings().mergeCells || []
  const mergeIndex = {}
  const mergeExcludeIndex = {}
  for(var k = 0; k < mergeCells.length; k++) {
    let {row, col, rowspan, colspan} = mergeCells[k]
    for(var i = row; i < row + rowspan; i++) {
      for(var j = col; j < col + colspan; j++) {
        if(i === row && j === col) {
          mergeIndex[i + ',' + j] = {rowspan, colspan}
          continue
        }
        mergeExcludeIndex[i + ',' + j] = true
      }
    }
  }
  copyData = {
    cut: cut,
    rowIndex: rowIndex,
    colIndex: colIndex,
    row2Index: row2Index,
    col2Index: col2Index,
    data: []
  }
  var index = 0
  for(let i = rowIndex; i <= row2Index; i++) {
    copyData.data.push([])
    for(let j = colIndex; j <= col2Index; j++) {
      if(!mergeExcludeIndex[i + ',' + j]) {
        let cell = this.context.getCell(i, j);
        if (cell) {
          cell = JSON.parse(JSON.stringify(cell))
          let merge = mergeIndex[i + ',' + j]
          if(merge) {
            cell.rowspan = merge.rowspan
            cell.colspan = merge.colspan
          }
          copyData.data[index].push(cell)
        }
      }
    }
    index++
  }
  if(rowIndex === row2Index && colIndex === col2Index) {
    buildecurrentCellAnimation()
  } else {
    let m = mergeIndex[rowIndex + ',' + colIndex]
    if(m) {
      if(rowIndex + m.rowspan - 1 === row2Index && colIndex + m.colspan - 1 === col2Index) {
        buildecurrentCellAnimation()
      }  else {
        buildecurrentAreaAnimation()
      }
    } else {
      buildecurrentAreaAnimation()
    }
  }
}

export function pasteCell() {
  if(!copyData || copyData.data.length === 0) {
    return
  }
  const selected = this.getSelected()
  var rowIndex = selected[0]
  var colIndex = selected[1]
  var row2Index = selected[2]
  var col2Index = selected[3]

  const mergeCells = this.getSettings().mergeCells || [];
  const oldMergeCells = JSON.parse(JSON.stringify(mergeCells))
  let firstCell = copyData.data[0][0]
  let rowOffset = rowIndex - (firstCell.rowNumber - 1)
  let colOffset = colIndex - (firstCell.columnNumber - 1)
  if(checkPasteDisabled(rowOffset, colOffset, mergeCells, this.context)) {
    return
  }
  const oldCells = []
  const newCells = []
  for(var i = 0; i < copyData.data.length; i++) {
    let row = copyData.data[i]
    for(var j = 0; j < row.length; j++) {
      let cell = row[j]
      let r = cell.rowNumber - 1 + rowOffset
      let c = cell.columnNumber - 1 + colOffset
      if(cell.rowspan && cell.colspan) {
        const newMergeItem = {
          row: r,
          col: c,
          rowspan: cell.rowspan,
          colspan: cell.colspan
        }
        mergeCells.push(newMergeItem)
      }
      let targetCell = this.context.getCell(r, c);
      if(targetCell) {
        oldCells.push({
          "cell": targetCell,
          "value": targetCell.value,
          "cellStyle": targetCell.cellStyle
        })
        targetCell.value = JSON.parse(JSON.stringify(cell.value))
        targetCell.cellStyle = JSON.parse(JSON.stringify(cell.cellStyle))
        newCells.push({
          "cell": targetCell,
          "value": targetCell.value,
          "cellStyle": targetCell.cellStyle
        })
      }
    }
  }
  if(copyData.cut) {
    for (let i = copyData.rowIndex; i <= copyData.row2Index; i++) {
      for (let j = copyData.colIndex; j <= copyData.col2Index; j++) {
        let cell = this.context.getCell(i, j)
        if (!cell) {
          continue;
        }
        let newCell = {
          rowNumber: cell.rowNumber,
          columnNumber: cell.columnNumber,
          expand: 'None',
          remove: true,
          value: {
            type: 'simple',
            value: ''
          },
          cellStyle: {
            fontSize: 10,
            forecolor: '0,0,0',
            fontFamily: '宋体',
            align: 'left',
            valign: 'middle',
          }
        }
        this.context.addCell(newCell)
      }
    }
    cleanCopyPasteDirty()
  }
  this.updateSettings({
    mergeCells
  })
  Handsontable.hooks.run(this, 'afterSelectionEnd', rowIndex, colIndex, row2Index, col2Index);
  this.render()
  const that = this
  undoManager.add({
    redo: function() {
      that.updateSettings({
        mergeCells
      })
      for(let i = 0; i < newCells.length; i++) {
        let newCell = newCells[i]
        newCell.cell.value = newCell.value
        newCell.cell.cellStyle = newCell.cellStyle
      }
      Handsontable.hooks.run(that, 'afterSelectionEnd', rowIndex, colIndex, row2Index, col2Index);
      that.render()
    },
    undo: function() {
      that.updateSettings({
        mergeCells:oldMergeCells
      })
      for(let i = 0; i < oldCells.length; i++) {
        let oldCell = oldCells[i]
        oldCell.cell.value = oldCell.value
        oldCell.cell.cellStyle = oldCell.cellStyle
      }
      Handsontable.hooks.run(that, 'afterSelectionEnd', rowIndex, colIndex, row2Index, col2Index);
      that.render()
    },
  })
}

export function cleanCopyPasteDirty() {
  if(Interval) {
    clearInterval(Interval)
  }
  copyData = null
  let ele = document.getElementById('copy_border_block')
  if(ele) {
    ele.innerHTML = ''
  }
}

function buildecurrentCellAnimation() {
  let ele = document.getElementById('copy_border_block')
  let htBorders = ele.parentNode
  let wtBorders = htBorders.querySelectorAll('.current')
  let left = parseInt(wtBorders[1].style.left) + 2
  let height = parseInt(wtBorders[1].style.height)
  let top = parseInt(wtBorders[0].style.top) + 1
  let width = parseInt(wtBorders[0].style.width)
  ele.style.left = left + 'px'
  ele.style.top = top + 'px'
  ele.innerHTML = `<svg id="dash_spin" width="${width}px" height="${height}px" style="pointer-events: none;"><path id="line_green" stroke="#1FBB7D" stroke-width="2" fill="none" d="m0,0 v${height} h${width} v-${height} h-${width} z"></path><path id="dash_white" stroke="#FFFFFF" stroke-width="2" fill="none" stroke-dasharray="6" stroke-dashoffset="8" d="m0,0 v${height} h${width} v-${height} h-${width} z"></path></svg>`
  animation()
}

function buildecurrentAreaAnimation() {
  let ele = document.getElementById('copy_border_block')
  let htBorders = ele.parentNode
  let wtBorders = htBorders.querySelectorAll('.area')
  let left = parseInt(wtBorders[1].style.left) + 1
  let height = parseInt(wtBorders[1].style.height) + 2
  let top = parseInt(wtBorders[0].style.top)
  let width = parseInt(wtBorders[0].style.width) + 2
  ele.style.left = left + 'px'
  ele.style.top = top + 'px'
  ele.innerHTML = `<svg id="dash_spin" width="${width}px" height="${height}px" style="pointer-events: none;"><path id="line_green" stroke="#1FBB7D" stroke-width="2" fill="none" d="m0,0 v${height} h${width} v-${height} h-${width} z"></path><path id="dash_white" stroke="#FFFFFF" stroke-width="2" fill="none" stroke-dasharray="6" stroke-dashoffset="8" d="m0,0 v${height} h${width} v-${height} h-${width} z"></path></svg>`
  animation()
}

function animation() {
  if(Interval) {
    clearInterval(Interval)
  }
  let path = document.getElementById('dash_white')
  let step = 2
  let index = 0
  Interval = setInterval(() => {
    index = index + step
    path.style.strokeDashoffset = index
    if(index === 14) {
      index = 0
    }
  }, 100)
}

function checkPasteDisabled(rowOffset, colOffset, mergeCells, context) {
  const mergeIndex = {}
  for(var k = 0; k < mergeCells.length; k++) {
    let {row, col, rowspan, colspan} = mergeCells[k]
    for(var i = row; i < row + rowspan; i++) {
      for(var j = col; j < col + colspan; j++) {
        if(i === row && j === col) {
          mergeIndex[i + ',' + j] = {rowspan, colspan}
        }
      }
    }
  }
  for(var i = 0; i < copyData.data.length; i++) {
    let row = copyData.data[i]
    for(var j = 0; j < row.length; j++) {
      let cell = row[j]
      let r = cell.rowNumber - 1 + rowOffset
      let c = cell.columnNumber - 1 + colOffset
      let merge = mergeIndex[r + ',' + c]
      // 目标单元格已经合并单元格且合并单元格不匹配复制单元格
      if(merge && merge.rowspan !== cell.rowspan && merge.rowspan !== cell.colspan) {
        Message({message: '不能对合并单元格作部分修改',type: 'error'})
        return false
      }
      if(cell.rowspan && cell.colspan) {
        for(var m = r; m < r + cell.rowspan; m++) {
          for(var n = c; n < c + cell.colspan; n++) {
            let merge = mergeIndex[m + ',' + n]
            if(merge) {
              Message({message: '不能对合并单元格作部分修改',type: 'error'})
              return true
            } else {
              let td = context.hot.getCell(m, n)
              if (td && td.style.display === 'none') {
                Message({message: '不能对合并单元格作部分修改',type: 'error'})
                return true
              }
            }
          }
        }
      }
    }
  }
  return false
}
